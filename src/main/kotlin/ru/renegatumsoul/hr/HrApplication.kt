package ru.renegatumsoul.hr

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class HrApplication

fun main(args: Array<String>) {
    SpringApplication.run(HrApplication::class.java, *args)
}
